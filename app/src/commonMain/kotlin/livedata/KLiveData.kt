package livedata

import kotlinx.atomicfu.atomic

expect open class KLiveData<T>() {

    open fun observeForever(block: (T?) -> Unit)
    open val value: T?
    fun observe(lifecycle: KLifeCycle, block: (T?) -> Unit)
    open fun hasActiveObservers(): Boolean
}

expect open class KMutableLiveData<T>() : KLiveData<T> {
    override var value: T?
}

open class SingleLiveDataEvent<T> : KMutableLiveData<T>() {

    private val pending = atomic<Boolean>(false)

    override fun observeForever(block: (T?) -> Unit) {
        if (hasActiveObservers()) {
            print("Observer added but only one will be notified")
        }

        super.observeForever {
            if (pending.compareAndSet(true, false)) {
                block(it)
            }
        }
    }

    override var value: T? = null
        get() = super.value
        set(newValue) {
            field = newValue
        }

    fun call() {
        value = null
    }
}