package viewmodel

import base.ViewModel
import livedata.KLiveData
import livedata.KMutableLiveData
import model.Exchanges
import repository.Repository

class ExchangesViewModel(val repos: Repository) : ViewModel() {

    private val emptyExchangeListTitle = "An empty history"

    private val _exchangeList = KMutableLiveData<List<Exchanges>>()
    val exchangeList: KLiveData<List<Exchanges>> = _exchangeList

    private val _emptyState = KMutableLiveData<Boolean>()
    val emptyState : KLiveData<Boolean> = _emptyState

    fun loadExchanges() {
        repos.getAllItems()?.let {
            if (it.isEmpty()){
                _emptyState.value = true
            } else {
                _exchangeList.value = it
            }
        }
    }

    fun getEmptyTitle() = emptyExchangeListTitle
}