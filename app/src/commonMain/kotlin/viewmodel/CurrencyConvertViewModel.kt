package viewmodel

import base.ViewModel
import database.formatAmount
import kotlinx.serialization.UnstableDefault
import livedata.KLiveData
import livedata.KMutableLiveData
import livedata.SingleLiveDataEvent
import model.Currency
import network.ResultObject
import repository.Repository

class CurrencyConvertViewModel(val repos: Repository) : ViewModel() {

    //currencies which use to converting
    private var fromCurrency: String = ""
    private var toCurrency: String = ""

    //using to report about errors and warnings and also for display the result of converting
    private val _messageState = KMutableLiveData<String>()
    val messageState: KLiveData<String> = _messageState

    //just display list of currencies
    private val _currenciesList = KMutableLiveData<List<Currency>>()
    val currencies: KLiveData<List<Currency>> = _currenciesList

    //display progress status
    private val _isProgressRun = KMutableLiveData<Boolean>()
    val isProgressRun: KLiveData<Boolean> = _isProgressRun

    //shows errors
    private val _errorState = KMutableLiveData<String>()
    val errorState: KLiveData<String> = _errorState

    val _errorStateEvent = SingleLiveDataEvent<String>()

    init {
        clear()
    }

    @UnstableDefault
    fun loadCurrencies() {
        //todo remove obs
        repos.getCurrencies().apply { observeForever { ro -> ro?.also { retrieveData(it) } } }
    }

    @UnstableDefault
    fun convertCurrency(amount: Double) {

        if (amount <= 0.0) {
            updateMessageState("Amount should be non zero")
            return
        }

        if (isFromEmpty() || isToEmpty()) {
            updateMessageState("Select from/to")
            return
        }

        _messageState.value = "Processing..."

        repos.convertCurrency(fromCurrency, toCurrency)
            .apply { observeForever { factor -> factor?.also { retrieveConvertedData(it, amount) } } }
    }

    private fun retrieveData(resultObject: ResultObject<List<Currency>>) {
        when (resultObject) {
            is ResultObject.SuccessResult -> {
                _isProgressRun.value = false
                val list = resultObject.getResult()
                _currenciesList.value = list
            }
            is ResultObject.Processing -> {
                _isProgressRun.value = true
            }
            is ResultObject.ErrorResult -> {
                _isProgressRun.value = false
                _errorState.value = resultObject.toString()
            }
        }
    }

    private fun retrieveConvertedData(resultObject: ResultObject<Double>, amount: Double) {
        when (resultObject) {
            is ResultObject.SuccessResult -> {
                _isProgressRun.value = false
                resultObject.getResult()?.also { format ->
                    var convertResult: String

                    if (format > 0.0) {
                        convertResult = formatAmount(amount * format)
                    } else {
                        convertResult = amount.toString()
                    }

                    _messageState.value = convertResult

                    repos.insertItem(
                        fromCurrency,
                        toCurrency,
                        amount.toString(),
                        convertResult
                    )

                    repos.setString(convertResult)
                }
            }
            is ResultObject.Processing -> {
                _isProgressRun.value = true
            }
            is ResultObject.ErrorResult -> {
                _isProgressRun.value = false
                _errorState.value = resultObject.toString()
            }
        }
    }

    fun isFromEmpty() = fromCurrency.isEmpty()

    fun isToEmpty() = toCurrency.isEmpty()

    fun setFromCurrency(from: String) {
        fromCurrency = from

        updateMessageState("Select to")
    }

    fun setToCurrency(to: String) {
        toCurrency = to

        updateMessageState("Enter amount and press convert")
    }

    private fun updateMessageState(message: String) {
        _errorStateEvent.value = message
        _messageState.value = message
    }

    fun clear() {
        updateMessageState("Select from")
    }
}