package deserializer

import kotlinx.serialization.*
import kotlinx.serialization.internal.SerialClassDescImpl
import kotlinx.serialization.json.JsonInput
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.doubleOrNull

class JsonConvertDeserializer() : DeserializationStrategy<Double> {
    override val descriptor: SerialDescriptor
        get() = SerialClassDescImpl("Convert")

    override fun deserialize(decoder: Decoder): Double {
        val input = decoder as? JsonInput ?: throw SerializationException("Expected Json Input")
        val jsonObject = input.decodeJson() as JsonObject

        val key = jsonObject.keys.first()
        return jsonObject[key]?.doubleOrNull ?: 1.0
    }

    override fun patch(decoder: Decoder, old: Double): Double {
        throw UpdateNotSupportedException("Fail to convert!")
    }
}