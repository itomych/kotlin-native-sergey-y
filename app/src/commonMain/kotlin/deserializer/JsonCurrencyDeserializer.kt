package deserializer

import kotlinx.serialization.*
import kotlinx.serialization.internal.SerialClassDescImpl
import kotlinx.serialization.json.*
import model.Currency

class JsonCurrencyDeserializer() : DeserializationStrategy<List<Currency>> {

    override val descriptor: SerialDescriptor =
        SerialClassDescImpl("Currencies")

    override fun deserialize(decoder: Decoder): List<Currency> {
        val input = decoder as? JsonInput ?: throw SerializationException("Expected Json Input")
        val jsonElement: JsonObject? = input.decodeJson() as? JsonObject
        val array = jsonElement?.getObject("results")

        return array?.map {
            val value = it.value as JsonObject
           Currency(value.getValue("id").toString().replace("\""," "))
        }?.toList() ?: listOf()
    }

    override fun patch(decoder: Decoder, old: List<Currency>) =
        throw UpdateNotSupportedException("Fail parse list of currencies")

}