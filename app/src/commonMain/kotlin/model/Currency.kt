package model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class Currency(
    @SerialName("id")
    val id: String?
)

