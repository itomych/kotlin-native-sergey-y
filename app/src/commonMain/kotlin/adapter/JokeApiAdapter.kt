package adapter

import io.ktor.client.HttpClient
import io.ktor.client.features.HttpClientFeature
import io.ktor.client.response.HttpReceivePipeline
import io.ktor.client.response.HttpResponse
import io.ktor.http.isSuccess
import io.ktor.util.AttributeKey
import kotlinx.io.errors.IOException

object JokeApiAdapter : HttpClientFeature<Unit, JokeApiAdapter> {
    override val key: AttributeKey<JokeApiAdapter> =
        AttributeKey("ApiAdapter")

    override fun install(feature: JokeApiAdapter, scope: HttpClient) {
        scope.responsePipeline.intercept(HttpReceivePipeline.After){
            val resp = subject.response as HttpResponse

            if (!resp.status.isSuccess())
                throw IOException("REQUEST !!!  ${resp.toString()}")

            proceedWith(subject)
        }

    }

    override fun prepare(block: Unit.() -> Unit): JokeApiAdapter {
        return this
    }
}