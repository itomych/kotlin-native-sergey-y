package repository

import database.DataBaseStorage
import database.Database
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.serialization.UnstableDefault
import livedata.KLiveData
import model.Currency
import network.NetworkData
import network.NetworkDataSource
import network.ResultObject
import preferenc.SharedPref
import preferenc.SharedStorage

internal expect val ApplicationDispatcher: CoroutineDispatcher

class Repository(
    private val sharedPref: SharedPref,
    private val database: Database,
    private val network: NetworkData
) : NetworkDataSource, DataBaseStorage, SharedStorage {

    @UnstableDefault
    override fun getCurrencies(): KLiveData<ResultObject<List<Currency>>?> =
        network.getCurrencies()

    @UnstableDefault
    override fun convertCurrency(from: String, to: String): KLiveData<ResultObject<Double>?> =
        network.convertCurrency(from, to)

    override fun getAllItems() =
        database.getAllItems()

    override fun insertItem(fromCurrency: String, toCurrency: String, amount: String, exchangeResult: String) {
        database.insertItem(fromCurrency, toCurrency, amount, exchangeResult)
    }

    override fun setString(string: String) {
        sharedPref.setString(string)
    }

    override fun getString(defaultValue: String) =
        sharedPref.getString(defaultValue)

    override fun getString() =
        sharedPref.getString()
}