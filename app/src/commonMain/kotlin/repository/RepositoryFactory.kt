package repository

import database.Database
import network.NetworkData
import preferenc.SharedPref

class RepositoryFactory {

    companion object{
        fun provideRepository(sharedPref: SharedPref, database: Database) : Repository{
            return Repository(sharedPref, database, NetworkData())
        }
    }
}