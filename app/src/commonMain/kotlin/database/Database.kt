package database

import com.squareup.sqldelight.db.SqlDriver
import db.currencydb.CurrencyDB
import model.CurrencyQueries
import model.Exchanges

expect var sqlDriver: SqlDriver?
expect fun setupDatabase(context: Any?)
expect fun formatAmount(amount: Double) : String

class Database {

    fun setup() {
        setupDatabase(null)
    }

    fun setup(context: Any) {
        setupDatabase(context)
    }

    private fun getTestQueries(): CurrencyQueries? {
        sqlDriver?.let { driver ->
            val db = CurrencyDB(driver)
            return db.currencyQueries
        }
        return null
    }

    fun getAllItems(): List<Exchanges>? {
        val testQueries = getTestQueries()

        return testQueries?.SelectAll()?.executeAsList()
    }

    fun insertItem(fromCurrency: String, toCurrency: String, amount: String, exchangeResult: String) {
        getTestQueries()?.InsertItem(fromCurrency, toCurrency,amount, exchangeResult)
    }
}