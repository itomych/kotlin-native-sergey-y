package database

import model.Exchanges

interface DataBaseStorage {

    fun getAllItems(): List<Exchanges>?

    fun insertItem(fromCurrency: String, toCurrency: String, amount: String, exchangeResult: String)
}