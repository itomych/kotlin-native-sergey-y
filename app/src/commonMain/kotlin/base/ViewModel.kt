package base

import kotlinx.coroutines.CoroutineScope

expect open class ViewModel() {

    protected val coroutineScope: CoroutineScope

    protected open fun onClear()
}