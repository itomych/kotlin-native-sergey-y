package network

import livedata.KLiveData
import model.Currency

interface NetworkDataSource {

     fun getCurrencies() : KLiveData<ResultObject<List<Currency>>?>

     fun convertCurrency(from: String, to: String) : KLiveData<ResultObject<Double>?>
}