package network

import deserializer.JsonConvertDeserializer
import deserializer.JsonCurrencyDeserializer
import io.ktor.client.HttpClient
import io.ktor.client.call.call
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.*
import io.ktor.client.request.forms.submitForm
import io.ktor.client.response.readBytes
import io.ktor.client.response.readText
import io.ktor.client.utils.EmptyContent
import io.ktor.http.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.io.core.use
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import livedata.KLiveData
import livedata.KMutableLiveData
import model.Currency
import repository.ApplicationDispatcher

class NetworkData {

    val BASE_URL = Url("https://free.currconv.com/api/v7/")
    val API_KEY = "d2ba0dea51c43ae6dad8"
    val REQUEST_API_KEY = "?apiKey=$API_KEY"
    val CURRENCIES = "currencies"
    val CONVERT = "convert?q="

    private val network = HttpClient() {
        expectSuccess = false
        install(JsonFeature)
    }

    @UnstableDefault
    fun getCurrencies(): KLiveData<ResultObject<List<Currency>>?> {
        return loadData { data ->
            GlobalScope.launch(ApplicationDispatcher) {
                data.value = network.runRequest(deserializer = JsonCurrencyDeserializer()) {
                    url("$BASE_URL$CURRENCIES$REQUEST_API_KEY")
                }
            }
        }
    }

    @UnstableDefault
    fun convertCurrency(from: String, to: String): KLiveData<ResultObject<Double>?> {
        return loadData { data ->
            GlobalScope.launch(ApplicationDispatcher) {
                data.value = network.runRequest(deserializer = JsonConvertDeserializer()) {
                    val conv = "$from _$to".replace(" ", "")
                    val url = "$BASE_URL$CONVERT$conv,${conv.reversed()}&compact=ultra&apiKey=$API_KEY"
                    url(url)
                }
            }
        }
    }

    @UnstableDefault
    private suspend inline fun <T : Any?> HttpClient.runRequest(
        scheme: String = "http", host: String = "localhost", port: Int = DEFAULT_PORT,
        path: String = "/",
        body: Any = EmptyContent,
        deserializer: DeserializationStrategy<T>,
        block: HttpRequestBuilder.() -> Unit = {}
    ): ResultObject<T> {
        try {
            val result = request {
                url(scheme, host, port, path)
                method = HttpMethod.Get
                this.body = body
                apply(block)
            } as String
            val list: T = Json.parse(deserializer, result)
            return ResultObject.SuccessResult(list)
        } catch (e: Exception) {
            return handleError(e)
        }
    }

    private fun <T> loadData(networkDataFetching: (KMutableLiveData<ResultObject<T>?>) -> Job): KMutableLiveData<ResultObject<T>?> {
        val data = KMutableLiveData<ResultObject<T>?>()
        val processing: ResultObject.Processing
        val job = networkDataFetching(data)
        processing = ResultObject.Processing(job)

        data.value = processing

        return data
    }

    private fun handleError(e: Exception): ResultObject.ErrorResult {
        return if (e is BadContentTypeFormatException) {
            //todo handle connection error
            ResultObject.ErrorResult(e)
        } else {
            ResultObject.ErrorResult(e)
        }
    }

    //test POST  --- test
    private fun testPost() {
        val URL = "https://httpbin.org/delay/2"

        val client = HttpClient()

        GlobalScope.launch(ApplicationDispatcher) {
//            val res = client.call {
//                url.takeFrom(URL)
//                method = HttpMethod.Post
//
//            }.response.readText()

            val params = Parameters.build {
                append("Star", "Sun")
            }

            val res = client.submitForm<String>(URL, params, encodeInQuery = false){
                headers{
                    append("test","2345")
                }
            }

            println("post result = $res")
        }
    }

    // ------- test -----
}