package network

import kotlinx.coroutines.Job

sealed class ResultObject<out T : Any?>(protected val resultData: T?) {

    class SuccessResult<out T : Any?>(result: T) : ResultObject<T>(result) {
        fun getResult() = resultData
    }

    open class ErrorResult(open val error: Throwable) : ResultObject<Nothing>(null)

    open class Processing(private val coroutineScope: Job?) : ResultObject<Nothing>(null) {
        fun cancelOperation() {
            coroutineScope?.cancel()
        }
    }
}