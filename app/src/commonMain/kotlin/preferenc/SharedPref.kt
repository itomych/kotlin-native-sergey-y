package preferenc

private const val BYTE_ARRAY = "byte_array"

expect class SharedPref(prefsName: String) {

    fun setByteArray(bytearr: ByteArray)
    fun getByteArray(defaultValue: ByteArray): ByteArray
    fun getByteArray(): ByteArray?

    fun setString(string: String)
    fun getString(defaultValue: String) : String
    fun getString() : String?
}