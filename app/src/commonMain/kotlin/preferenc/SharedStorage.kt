package preferenc

interface SharedStorage {

    fun setString(string : String)
    fun getString(defaultValue: String) : String
    fun getString() : String?
}