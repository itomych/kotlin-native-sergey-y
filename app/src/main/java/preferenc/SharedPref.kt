package preferenc

import android.content.Context
import android.content.SharedPreferences
import sample.App

actual class SharedPref actual constructor(prefsName: String) {

    private val KEY_BYTE_ARRAY = "byte_array"
    private val KEY_STRING = "string"

    private val sp: SharedPreferences? =
       App.instance.getSharedPreferences(prefsName, Context.MODE_PRIVATE)

    actual fun setByteArray(bytearr: ByteArray) {
        sp?.edit()?.putString(KEY_BYTE_ARRAY, bytearr.toString())?.apply()
    }

    actual fun getByteArray(defaultValue: ByteArray): ByteArray {
        val string = sp?.getString(KEY_BYTE_ARRAY, "")

        return if (string == "") {
            defaultValue
        } else {
            string!!.toByteArray()
        }
    }

    actual fun getByteArray(): ByteArray? {
        return sp?.getString(KEY_BYTE_ARRAY, "")?.toByteArray()
    }

    actual fun setString(string: String) {
        sp?.edit()?.putString(KEY_STRING, string)?.apply()
    }

    actual fun getString(defaultValue: String): String {
        return sp?.getString(KEY_STRING, defaultValue) ?: defaultValue
    }

    actual fun getString(): String? {
        return sp?.getString(KEY_STRING, "")
    }
}