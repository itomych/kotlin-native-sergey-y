package database

import android.content.Context
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver
import db.currencydb.CurrencyDB
import java.math.BigDecimal
import java.text.DecimalFormat

actual var sqlDriver: SqlDriver? = null
actual fun setupDatabase(context: Any?) {
    context?.let {
        sqlDriver = AndroidSqliteDriver(CurrencyDB.Schema, it as Context, "1.db")
    }
}

//actual fun createDB(): TestDB {
//    val driver = AndroidSqliteDriver(TestDB.Schema, , "1.db")
//    return TestDB(driver)
//}
actual fun formatAmount(amount: Double) =
    String.format("%.${2}f", amount)
