package base


import android.arch.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel

actual open class ViewModel actual constructor() : ViewModel() {
    protected actual val coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.Main)

    protected actual open fun onClear() {
        super.onCleared()

        coroutineScope.cancel()
    }
}