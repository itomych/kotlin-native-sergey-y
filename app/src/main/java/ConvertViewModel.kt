import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import common.SingleLiveEvent
import kotlinx.serialization.UnstableDefault
import livedata.KMutableLiveData
import model.Currency
import repository.Repository


class ConvertViewModel : ViewModel() {

    val titleOfScreen = MutableLiveData<String>()
    val currencies = MutableLiveData<List<Currency>>()
    val message = SingleLiveEvent<String>()
    private var from: String? = ""
    private var to: String? = ""

    @UnstableDefault
    fun start() {
        titleOfScreen.value = "Currency from"
    }

    fun convert(amount: String) {

        if (amount.isEmpty() || amount.toDouble() <= 0.0) {
            message.value = "Should write correct amount"
            return
        } else if (from?.isEmpty() == true || to?.isEmpty() == true) {
            message.value = "Should to select from/to currency"
            return
        }

        from?.let { fr ->
//            to?.let { t ->
//                repository.convertCurrency(fr, t) {
//                    // titleOfScreen.value = "Converted $it"
//                    titleOfScreen.postValue((total * it).toString())
//                }
//            } ?: titleOfScreen.postValue("Choodse to !!!")
        } ?: titleOfScreen.postValue("Choodse form !!!")
    }

    fun isCurrencyFromEmpty() = from?.isEmpty()

    fun setCurrencyFrom(from: String?) {
        this.from = from
        titleOfScreen.postValue("Currency to")
        message.value = "from $from"
    }

    fun setCurrencyTo(to: String?) {
        this.to = to
        titleOfScreen.postValue("Press convert")
        message.value = "to $to"
    }

    fun clear() {
        from = ""
        to = ""
        message.value = ""
        titleOfScreen.value = "Currency from"
    }
}