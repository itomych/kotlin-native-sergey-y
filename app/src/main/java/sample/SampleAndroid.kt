package sample

import android.content.Context
import android.os.Bundle

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import database.Database
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item.view.*
import kotlinx.serialization.UnstableDefault
import livedata.kLifeCycle
import model.Currency
import preferenc.SharedPref
import repository.RepositoryFactory
import viewmodel.CurrencyConvertViewModel


interface ItemClick {
    fun onItemClick(currency: String?)
}

class MainActivity : AppCompatActivity(), ItemClick {

    private val viewModel: CurrencyConvertViewModel by lazy {
        CurrencyConvertViewModel(RepositoryFactory.provideRepository(
            SharedPref("amdroid"),
            Database().apply { setup(this@MainActivity) }
        ))
    }

    private val listAdapter = ListAdapter().apply {
        setClickListener(this@MainActivity)
    }

    @UnstableDefault
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        observe()

        rv_list.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            this.adapter = listAdapter
        }

        btn_Convert.setOnClickListener {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
            val amount: Double? = et_amount.text?.let { if (it.isEmpty()) 0.0 else it.toString().toDouble() }
            viewModel.convertCurrency(amount ?: 0.0)
            et_amount.clearComposingText()
        }

        btn_history.setOnClickListener {
            startActivity(ExchangesActivity.newIntent(this@MainActivity))
        }

        viewModel.loadCurrencies()
    }

    private fun observe() {
        viewModel.run {

            messageState.observe(this@MainActivity.kLifeCycle()) {
                tv_current_currency.text = it
            }

            currencies.observe(this@MainActivity.kLifeCycle()) {
                listAdapter.items = it ?: listOf()
                listAdapter.notifyDataSetChanged()
            }

            isProgressRun.observe(this@MainActivity.kLifeCycle()) {
                it?.run {
                    pb_loading.visibilityByBool(this)
                    rv_list.visibilityByBool(!this)
                }
            }

            _errorStateEvent.observe(this@MainActivity.kLifeCycle()){
                Log.w("TEST", it)
            }
        }
    }

    override fun onItemClick(currency: String?) {
        if (viewModel.isFromEmpty()) {
            viewModel.setFromCurrency(currency ?: "")
        } else {
            viewModel.setToCurrency(currency ?: "")
        }
    }

    class ListAdapter(var items: List<Currency> = arrayListOf()) : RecyclerView.Adapter<ListAdapter.ItemVH>() {

        private var inflater: LayoutInflater? = null
        private var itemClickCallBack: ItemClick? = null

        override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemVH {
            val v: View = getInflater(parent.context).inflate(R.layout.item, parent, false)
            return ItemVH(v)
        }

        override fun getItemCount() = items.size
        override fun onBindViewHolder(holder: ItemVH, position: Int) {
            holder.bind(items[position])
        }

        inner class ItemVH(val view: View) : RecyclerView.ViewHolder(view) {

            fun bind(data: Currency?) {
                data?.run {
                    view.item_title.text = this.id
                    view.setOnClickListener {
                        itemClickCallBack?.onItemClick(this.id)
                    }
                }
            }
        }

        private fun getInflater(context: Context): LayoutInflater {
            return inflater ?: LayoutInflater.from(context).also {
                inflater = it
            }
        }

        fun setClickListener(callBack: ItemClick) {
            this.itemClickCallBack = callBack
        }
    }
}

fun View.visibilityByBool(bool: Boolean) {
    this.visibility = if (bool) View.VISIBLE else View.GONE
}