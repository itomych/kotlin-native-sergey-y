package sample

import android.content.Context
import android.content.Intent
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import database.Database
import kotlinx.android.synthetic.main.activity_exchanges.*
import kotlinx.android.synthetic.main.item_history.view.*
import model.Exchanges
import preferenc.SharedPref
import repository.RepositoryFactory
import viewmodel.ExchangesViewModel

class ExchangesActivity : AppCompatActivity() {

    private val viewModel: ExchangesViewModel by lazy {
        ExchangesViewModel(
            RepositoryFactory.provideRepository(
                SharedPref("android"),
                Database().apply { setup(this@ExchangesActivity) })
        )
    }

    private val exchangeAdapter = ExchangeAdapter(listOf())

    companion object {
        fun newIntent(ctx: Context) = Intent(ctx, ExchangesActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exchanges)

        rv_exchanges.apply {
            layoutManager = LinearLayoutManager(this@ExchangesActivity)
            adapter = exchangeAdapter
        }

        observe()
        loadData()
    }

    private fun observe() {
        viewModel.let { vm ->
            vm.exchangeList.observeForever { list ->
                list?.let {
                    exchangeAdapter.setItems(it)
                }
            }

            vm.emptyState.observeForever {
                tv_emptyHistory.text = vm.getEmptyTitle()
            }
        }
    }

    private fun loadData() {
        viewModel.loadExchanges()
    }

    private class ExchangeAdapter(private var items: List<Exchanges>) :
        RecyclerView.Adapter<ExchangeAdapter.ExchangesVH>() {

        private var inflater: LayoutInflater? = null

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ExchangesVH {
            val view = getInflater(p0.context).inflate(R.layout.item_history, p0, false)
            return ExchangesVH(view)
        }

        override fun getItemCount() = items.size

        override fun onBindViewHolder(p0: ExchangesVH, p1: Int) {
            p0.bind(items[p1])
        }

        fun setItems(items: List<Exchanges>) {
            this.items = items
            notifyDataSetChanged()
        }

        private fun getInflater(ctx: Context): LayoutInflater =
            inflater ?: LayoutInflater.from(ctx).also {
                inflater = it
            }

        inner class ExchangesVH(val view: View) : RecyclerView.ViewHolder(view) {
            fun bind(data: Exchanges) {
                view.tv_exchange_item.text = generateString(data)
            }

            private fun generateString(ex: Exchanges) =
                StringBuffer().append("from ${ex.fromCurrency} to ${ex.toCurrency} \n amount ${ex.amount} result ${ex.exchangeResult}").toString()
        }
    }
}