package livedata

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import java.lang.RuntimeException

private val lifecycleOwnerTag = "LifecycleOwner"

actual open class KLiveData<T> actual constructor() {

    internal val liveData =  MutableLiveData<T>()
    actual open val value: T?
        get() = liveData.value

    actual open fun observeForever(block: (T?) -> Unit) {
        liveData.observeForever {
            block(it)
        }
    }

    actual fun observe(lifecycle: KLifeCycle, block: (T?) -> Unit) {
        val lifecycleOwner = lifecycle.tags[lifecycleOwnerTag]

        if (lifecycleOwner == null || lifecycleOwner !is LifecycleOwner) {
            throw RuntimeException("Use LifecycleOwner.kLifeCycle()")
        } else {
            lifecycle.tags.remove(lifecycleOwnerTag)
            observe(lifecycleOwner, block)
        }
    }

    actual open fun hasActiveObservers(): Boolean {
        return liveData.hasActiveObservers()
    }
}

actual open class KMutableLiveData<T> actual constructor() : KLiveData<T>() {
    actual override var value: T?
        get() = liveData.value
        set(value) {
            liveData.postValue(value)
        }
}

fun LifecycleOwner.kLifeCycle(): KLifeCycle {
    val kLifeCycle = KLifeCycle()
    kLifeCycle.tags[lifecycleOwnerTag] = this
    return kLifeCycle
}

fun <T> KLiveData<T>.observe(lifecycle: LifecycleOwner, block: (T?) -> Unit){
    this.liveData.observe(lifecycle, Observer<T> {
        block(it)
    })
}