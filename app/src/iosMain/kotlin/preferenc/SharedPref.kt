package preferenc

import platform.Foundation.NSUserDefaults

actual class SharedPref actual constructor(prefsName: String) {

    private val BYTE_ARRAY = "array"
    private val KEY_STRING = "string"

    private val userDefault: NSUserDefaults = NSUserDefaults(suiteName = prefsName)

    actual fun setByteArray(bytearr: ByteArray) {
        userDefault.setObject(bytearr, BYTE_ARRAY)
    }

    actual fun getByteArray(defaultValue: ByteArray): ByteArray {
        return userDefault.objectForKey(BYTE_ARRAY) as ByteArray? ?: defaultValue
    }

    actual fun getByteArray(): ByteArray? {
        return userDefault.objectForKey(BYTE_ARRAY) as ByteArray?
    }

    actual fun setString(string: String) {
        userDefault.setObject(string, KEY_STRING)
    }

    actual fun getString(defaultValue: String): String {

        val objectForKey = userDefault.objectForKey(KEY_STRING) as String
        return if (objectForKey.isEmpty()) {
            defaultValue
        } else {
            objectForKey
        }
    }

    actual fun getString(): String? {
        return userDefault.objectForKey(KEY_STRING) as String
    }
}