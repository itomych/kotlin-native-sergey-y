package base

import database.Database
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import preferenc.SharedPref
import repository.ApplicationDispatcher
import repository.Repository
import repository.RepositoryFactory

actual open class ViewModel actual constructor() {
    protected actual val coroutineScope: CoroutineScope = CoroutineScope(ApplicationDispatcher)

    protected actual open fun onClear() {
        coroutineScope.cancel()
    }
}