package database

import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.drivers.ios.NativeSqliteDriver
import db.currencydb.CurrencyDB
import platform.Foundation.NSString
import platform.Foundation.stringWithFormat

actual var sqlDriver: SqlDriver? = null

actual fun setupDatabase(context: Any?) {
    sqlDriver = NativeSqliteDriver(CurrencyDB.Schema, "1.db")
}

actual fun formatAmount(amount: Double) =
    NSString.stringWithFormat("%.${2}f", amount)

